<?php
// +----------------------------------------------------------------------
// | Author: 冰蓝工作室
// +----------------------------------------------------------------------
// | Email: zhouyong_0@hotmail.com
// +----------------------------------------------------------------------
// | Date: 2021/11/17 22:00
// +----------------------------------------------------------------------
// | DESC: 文件名称NodeAnotation.php
// +----------------------------------------------------------------------
// | Copyright (c) 2021-2025 rights reserved.
// +----------------------------------------------------------------------
namespace Iceblue\IceAdmin\annotation;

use Doctrine\Common\Annotations\Annotation\Attributes;


/**
 * 创建节点注解类
 * @Annotation
 * @Target({"METHOD","CLASS"})
 * @Attributes({
        @Attribute("time", type = "int")
 * })
 */
final class NodeAnotation
{
    /**
     * 节点名称
     * @Required()
     * @var string
     */
    public $title;

    /**
     * 是否开启权限控制
     * @Enum({true,false})
     * @var bool
     */
    public $auth = true;
}