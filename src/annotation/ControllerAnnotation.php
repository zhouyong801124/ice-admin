<?php
// +----------------------------------------------------------------------
// | Author: 冰蓝工作室
// +----------------------------------------------------------------------
// | Email: zhouyong_0@hotmail.com
// +----------------------------------------------------------------------
// | Date: 2021/11/17 22:00
// +----------------------------------------------------------------------
// | DESC: 文件名称ControllerAnnotation.php
// +----------------------------------------------------------------------
// | Copyright (c) 2021-2025 rights reserved.
// +----------------------------------------------------------------------
namespace Iceblue\IceAdmin\annotation;

use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Required;
use Doctrine\Common\Annotations\Annotation\Target;


/**
 * 控制器注释扩展常量
 * Class ControllerAnnotation
 * @Annotation
 * @Target("CLASS")
 * @Attributes ({
        @Attribute("title", type="string"),
 * })
 *
 * @since 2.0
 */
final class ControllerAnnotation
{
    /**
     * 控制器路由组前缀
     * @Required()
     * @var string
     */
    public $title = '';

    /**
     * 是否开启权限控制
     * @Enum({true,false})
     * @var bool
     */
    public $auth = true;
}