<?php
// +----------------------------------------------------------------------
// | Author: 冰蓝工作室
// +----------------------------------------------------------------------
// | Email: zhouyong_0@hotmail.com
// +----------------------------------------------------------------------
// | Date: 2021/11/17 22:00
// +----------------------------------------------------------------------
// | DESC: 文件名称Local.php
// +----------------------------------------------------------------------
// | Copyright (c) 2021-2025 rights reserved.
// +----------------------------------------------------------------------
namespace Iceblue\IceAdmin\upload\driver;

use Iceblue\IceAdmin\upload\FileBase;
use Iceblue\IceAdmin\upload\trigger\SaveDb;

/**
 * 本地上传
 * Class Local
 * @package iceblue\upload\driver
 */
class Local extends FileBase
{

    /**
     * 重写上传方法
     * @return array|void
     */
    public function save()
    {
        parent::save();
        SaveDb::trigger($this->tableName, [
            'upload_type'   => $this->uploadType,
            'original_name' => $this->file->getOriginalName(),
            'mime_type'     => $this->file->getOriginalMime(),
            'file_ext'      => strtolower($this->file->getOriginalExtension()),
            'url'           => $this->completeFileUrl,
            'create_time'   => time(),
        ]);
        return [
            'save' => true,
            'msg'  => '上传成功',
            'url'  => $this->completeFileUrl,
        ];
    }

}