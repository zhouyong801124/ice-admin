<?php
// +----------------------------------------------------------------------
// | Author: 冰蓝工作室
// +----------------------------------------------------------------------
// | Email: zhouyong_0@hotmail.com
// +----------------------------------------------------------------------
// | Date: 2021/11/17 22:00
// +----------------------------------------------------------------------
// | DESC: 文件名称Txcos.php
// +----------------------------------------------------------------------
// | Copyright (c) 2021-2025 rights reserved.
// +----------------------------------------------------------------------
namespace Iceblue\IceAdmin\upload\driver;

use Iceblue\IceAdmin\upload\FileBase;
use Iceblue\IceAdmin\upload\driver\txcos\Cos;
use Iceblue\IceAdmin\upload\trigger\SaveDb;


/**
 * 腾讯云上传
 * Class Txcos
 * @package EasyAdmin\upload\driver
 */
class Txcos extends FileBase
{
    /**
     * 重写上传方法
     * @return array|void
     */
    public function save()
    {
        parent::save();
        $upload = Cos::instance($this->uploadConfig)
            ->save($this->completeFilePath, $this->completeFilePath);
        if ($upload['save'] == true) {
            SaveDb::trigger($this->tableName, [
                'upload_type'   => $this->uploadType,
                'original_name' => $this->file->getOriginalName(),
                'mime_type'     => $this->file->getOriginalMime(),
                'file_ext'      => strtolower($this->file->getOriginalExtension()),
                'url'           => $upload['url'],
                'create_time'   => time(),
            ]);
        }
        $this->rmLocalSave();
        return $upload;
    }
}